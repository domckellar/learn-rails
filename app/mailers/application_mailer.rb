class ApplicationMailer < ActionMailer::Base
  default from: "dom.mckellar@gmail.com"
  layout 'mailer'
end
